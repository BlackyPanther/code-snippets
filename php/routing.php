<?php

/**
 * routing.php
 * (c) Andreas Mueller <webmaster@am-wd.de>
 */

/**
 * get_route
 * 
 * Parses the requested route and retrieves the matching properties.
 * Inspired by the ASP.NET Core routing feature.
 * 
 * Example:
 * template: image/{album}/{id?}
 * Url:      index.php/image/London 2008/14
 * Result:   album => London 2008, id => 14
 * 
 * @author     Andreas Mueller <webmaster@am-wd.de>
 * @copyright  (c) 2016 Andreas Mueller
 * @license    MIT - http://am-wd.de/index.php?p=about#license
 * @see        https://docs.asp.net/en/latest/fundamentals/routing.html
 *
 * @param string  $template  The template to search the path for
 *
 * @return mixed  an associative array with the properties or null in case of an error.
 */
function get_route($template)
{
	$trace = debug_backtrace()[0];
	
	$path = isset($_SERVER['PATH_INFO']) ? substr($_SERVER['PATH_INFO'], 1) : '';
	$arPath = explode('/', $path);
	$arRoute = explode('/', $template);
	
	$result = array();
	$routeLength = count($arRoute);
	$optionalFound = false;
	
	for ($i = 0; $i < $routeLength; $i++)
	{
		if (preg_match("/{(.*)}/", $arRoute[$i], $match))
		{
			if (substr($match[1], -1) == '?')
			{
				$key = substr($match[1], 0, strlen($match[1]) - 1);
				$value = isset($arPath[$i]) ? $arPath[$i] : null;
				$optionalFount = true;
			}
			else if ($optionalFound)
			{
				$msg = 'Required route segment found after optional in '.$trace['file'].':'.$trace['line'];
				user_error($msg, E_USER_ERROR);
				return null;
			}
			else
			{
				$key = $match[1];
				if (isset($arPath[$i]))
				{
					$value = $arPath[$i];
				}
				else
				{
					$msg = 'Required route segment "'.$key.'" not found in '.$trace['file'].':'.$trace['line'];
					user_error($msg, E_USER_ERROR);
					return null;
				}
			}
			
			if (preg_match("/^([\d]+)$/", $value))
			{
				$result[$key] = intval($value);
			}
			else
			{
				$result[$key] = $value;
			}
		}
		else
		{
			if (!isset($arPath[$i]) || $arRoute[$i] != $arPath[$i])
			{
				$msg = 'Static segment "'.$arRoute[$i].'" of route not found in '.$trace['file'].':'.$trace['line'];
				user_error($msg, E_USER_ERROR);
			}
		}
	}
	
	return $result;
}

?>