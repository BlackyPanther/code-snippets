<?php

/**
 * curl_exec_follow_location.php
 * (c) Andreas Mueller <webmaster@am-wd.de>
 */

function_exists('curl_exec') || user_error('php-curl not found, but it\'s a dependency.', E_USER_ERROR);

/**
 * online_available
 * 
 * Tries to connect to the given url via curl.
 * 
 * @author     Andreas Mueller <webmaster@am-wd.de>
 * @copyright  (c) 2016 Andreas Mueller
 * @license    MIT - http://am-wd.de/index.php?p=about#license
 *
 * @param   string  $url      Hostname/Server (URL) or IP address.
 * @param   int     $timeout  Time for response in milliseconds.
 *
 * @return  bool              True on successful connect, false on error (timeout).
 */
function online_available(string $url, int $timeout = 1000)
{
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, $timeout);

	curl_exec($ch);

	if (curl_errno($ch)) {
		curl_close($ch);
		return false;
	}
	curl_close($ch);

	return true;
}

?>