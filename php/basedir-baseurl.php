<?php

/**
 * basedir-baseurl.php
 *
 * The lines below define the directory of this file as BASEDIR
 * and refering to the BASEDIR and the diff with the _SERVER['DOCUMENT_ROOT']
 * the BASEURL is composed.
 */

// Define the current directory as base directory
define('BASEDIR', __DIR__);

// Corresponding to the current directory, define the url as base url
$url = str_replace($_SERVER['DOCUMENT_ROOT'], 'http://'.$_SERVER['HTTP_HOST'], BASEDIR);
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') $url = str_replace("http", "https", $url);
define('BASEURL', $url);

?>