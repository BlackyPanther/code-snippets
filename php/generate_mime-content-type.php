<?php

$url = 'https://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types';
$file = file_get_contents($url);

$lines = explode(PHP_EOL, $file);

header('Content-Type: text/plain; charset=utf-8');

$mimes = array();
foreach ($lines as $line)
{
	if (substr($line, 0, 1) == '#')
		continue;
	
	$ar = explode("\t", $line);
	if (trim($ar[0]) == '')
		continue;
	
	$extensions = explode(' ', $ar[(count($ar) - 1)]);
	foreach ($extensions as $ext)
	{
		if (trim($ext) == '')
			continue;
		
		$mimes[$ext] = trim($ar[0]);
	}
}

ksort($mimes);

$output = array();
$output[] = '<?php';
$output[] = '';
$output[] = '// Mapping MIME-Types and extensions.';
$output[] = '// Source: '.$url;
$output[] = '';
$output[] = '$mime_types = array();';

foreach ($mimes as $ext => $mime)
	$output[] = '$mime_types["'.$ext.'"] = "'.$mime.'";';

$output[] = '';

$output[] = 'if (!function_exists("mime_content_type"))';
$output[] = '{';
$output[] = '	function mime_content_type($filename)';
$output[] = '	{';
$output[] = '		$path = realpath($filename);';
$output[] = '		if (function_exists("pathinfo"))';
$output[] = '		{';
$output[] = '			$info = pathinfo($path);';
$output[] = '			$ext = $info["extension"];';
$output[] = '		}';
$output[] = '		else';
$output[] = '		{';
$output[] = '			$tmp = explode(".", basename($path));';
$output[] = '			$ext = $tmp[(count($tmp) - 1)];';
$output[] = '		}';
$output[] = '		return array_key_exists($ext, $mime_types) ? $mime_types[$ext] : "application/octet-stream";';
$output[] = '	}';
$output[] = '}';
$output[] = '';
$output[] = '?>';

echo implode(PHP_EOL, $output);

?>