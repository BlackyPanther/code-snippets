<?php

/**
 * api.php
 * (c) Andreas Mueller <webmaster@am-wd.de>
 */

/**
 * Provides a backend for the php_api.js function.
 * 
 * @author     Andreas Mueller <webmaster@am-wd.de>
 * @copyright  (c) 2016 Andreas Mueller
 * @license    MIT - http://am-wd.de/index.php?p=about#license
 */

// Report everything just to be sure it's clean code.
@error_reporting(E_ALL ^ E_NOTICE);
@ini_set('display_errors', 'on');

// Define output content type for errors.
header('Content-Type: text/html;charset=utf-8');

// Load all API functions in subfiles.
// These files are named like xxx.api.php
$dir = scandir(__DIR__);
foreach ($dir as $file)
{
	if ($file != 'api.php' && strpos($file, '.api.php') !== false)
		include_once __DIR__.'/'.$file;
}


// Get JSON request and decode it.
$params = json_decode(file_get_contents('php://input'));

// Prepare response.
$response = new stdClass();
$response->error = '';
$response->data = '';

// Try to execute the requested function.
if (function_exists($params->func))
{
	try
	{
		$response->data = @call_user_func($params->func, $params->data);
	}
	catch (Exception $ex)
	{
		$response->error = $ex->getMessage();
	}
}
else
{
	$response->error = 'Function "'.$params->func.'" not found.';
}

// Write proper response.
header('Content-Type: application/json;charset=utf-8');
echo json_encode($response);

?>