<?php

/**
 * curl_exec_follow_location.php
 * (c) Andreas Mueller <webmaster@am-wd.de>
 */

function_exists('curl_exec') || user_error('php-curl not found, but it\'s a dependencie.', E_USER_ERROR);

/**
 * curl_exec_follow_location
 *
 * executes curl_exec without security bug of
 * CURLOPT_FOLLOWLOCATION and open_basedir.
 * 
 * @author     Andreas Mueller <webmaster@am-wd.de>
 * @copyright  (c) 2016 Andreas Mueller
 * @license    MIT - http://am-wd.de/index.php?p=about#license
 *
 * @param resource  $ch         curl handler
 * @param int       $maxredirs  max. number of redirects; default -1, unlimited
 *
 * @return mixed  result of curl_exec()
 */
function curl_exec_follow_location(resource $ch, int $maxredirs = -1)
{
	if (ini_get('open_basedir') === '' && ini_get('safe_mode') === 'Off')
	{
		// Requirements fit to native follow location
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $maxredirs != 0);

		if ($maxredirs > 0)
			curl_setopt($ch, CURLOPT_MAXREDIRS, $maxredirs);

	}
	else
	{
		// need to use a workaround
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
		$origin = $url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
		$cp = curl_copy_handle($ch);

		curl_setopt($cp, CURLOPT_HEADER, true);
		curl_setopt($cp, CURLOPT_FORBID_REUSE, false);
		curl_setopt($cp, CURLOPT_RETURNTRANSFER, true);

		$run = $maxredirs != 0;
		$mr = $maxredirs;

		while ($run)
		{
			curl_setopt($cp, CURLOPT_URL, $url);
			$header = curl_exec($cp);

			if (curl_errno($cp))
			{
				$run = false;
			}
			else
			{
				$code = curl_getinfo($cp, CURLINFO_HTTP_CODE);
				if ($code == 301 || $code == 302)
				{
					preg_match("/Location:(.*?)\n/i", $header, $matches);
					$url = trim(array_pop($matches));

					if (!preg_match("/^https?:/i", $url))
						$url = $origin.$url;

				}
				else
				{
					$run = false;
				}
			}

			// max jumps reached
			$mr--;
			if ($maxredirs > 0 && $mr == 0)
				$run = false;
		}

		curl_close($cp);

		if ($maxredirs == -1 || ($maxredirs > 0 && $mr > 0))
			curl_setopt($ch, CURLOPT_URL, $url);

	}

	return curl_exec($ch);
}

?>