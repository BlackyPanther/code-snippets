# Code Snippets

-----

## CSS
Collection of some styles I normally use on my sites (in variations).

## JS
Some useful functions to make things easier to validate or visualize.

## PHP
Functions to fix some problems or enhance usabillity.

## SH
Commonly needed lines of code while scripting bash.

## SQL
SQL Statements or functions, that may be useful.

-----

### LICENSE
My scripts are published under [MIT License](https://am-wd.de/?p=about#license).