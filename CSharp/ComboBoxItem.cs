using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace AMWD.Snippets
{
	/// <summary>
	/// More flexible item for an <see cref="ComboBox"/>.
	/// </summary>
	/// <typeparam name="T">Datatype of the containing value.</typeparam>
	public class ComboBoxItem<T>
	{
		/// <summary>
		/// Gets or sets the label of the ComboBoxItem.
		/// </summary>
		public string Label { get; set; }

		/// <summary>
		/// Gets or sets the value of the ComboBoxItem.
		/// </summary>
		public T Value { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="ComboBoxItem{T}"/> class.
		/// </summary>
		public ComboBoxItem()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ComboBoxItem{T}"/> class using the given label and value.
		/// </summary>
		/// <param name="label">The label of the ComboBoxItem, that will be displayed.</param>
		/// <param name="value">The value the ComboBoxItem should contain.</param>
		public ComboBoxItem(string label, T value)
		{
			Label = label;
			Value = value;
		}

		/// <inheritdoc>
		public override string ToString()
		{
			return Label;
		}

		/// <inheritdoc>
		public override int GetHashCode()
		{
			return Label.GetHashCode() ^ Value.GetHashCode();
		}

		/// <inheritdoc>
		public override bool Equals(object obj)
		{
			ComboBoxItem<T> combobox = obj as ComboBoxItem<T>;

			return combobox != null &&
				Label == combobox.Label &&
				Value.Equals(combobox.Value);
		}
	}

	/// <summary>
	/// Small static class to extend the ComboBox functionality.
	/// </summary>
	public static class ComboBoxExtension
	{
		/// <summary>
		/// Adds a list of elements to an <see cref="ComboBox"/> items collection.
		/// </summary>
		/// <typeparam name="T">The type of the items to add.</typeparam>
		/// <param name="collection">The <see cref="ComboBox"/> items collection.</param>
		/// <param name="items">The items to add to the collection.</param>
		/// <param name="displayLabel">Function to define the text to display.</param>
		public static void AddRange<T>(this ComboBox.ObjectCollection collection, IEnumerable<T> items, Func<T, string> displayLabel = null)
		{
			foreach (T item in items)
			{
				string label = displayLabel?.Invoke(item) ?? item.ToString();
				collection.Add(new ComboBoxItem<T>(label, item));
			}
		}
	}
}