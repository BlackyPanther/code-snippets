#!/usr/bin/env bash

if [ -f $file ]; then
	echo "regular file $file exists";
fi

if [ -x $files ]; then
	echo "file $file exists and is executable";
fi

if [ -d $dir ]; then
	echo "directory $dir exits";
fi

if [ -z "$string" ]; then
	echo "string is empty";
fi

if [ -n "$string" ]; then
	echo "string is not empty";
if