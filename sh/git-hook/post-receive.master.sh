#!/usr/bin/env bash

# ========================================
# (c) Andreas Mueller <webmaster@am-wd.de>
#
# Execute what ever you want to be done
# ========================================

MAILBIN=$(which mail)

if [ -n "$MAILBIN" ]; then
	mail -s "Git - Push to Master" "user@domain.tld" <<< EOF
A new push to your branch MASTER has been detected.

You can review the changes here: http://domain.tld/to/your/gitlab
EOF
fi

WEBHOOKURL="http://domain.tld/your/webhook.php?token=abcdef";
curl $WEBHOOK &> /dev/null