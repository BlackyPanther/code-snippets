#!/bin/sh

# Source: incoming requests
SRCIFACE=eth0
SRCIP=172.16.0.12

# Destination: forwarding requests to
DSTIFACE=eth1
DSTIP=192.168.1.43

if [ "$1" = "start" ]; then
	echo "Setting iptables up"
	
	iptables -t nat -A POSTROUTING --out-interface ${DSTIFACE} -j MASQUERADE  
	iptables -A FORWARD --in-interface ${SRCIFACE} -j ACCEPT
	
	iptables -t nat -A PREROUTING -d ${SRCIP} -j DNAT --to-destination ${DSTIP}
	iptables -t nat -A POSTROUTING -s ${DSTIP} -j SNAT --to-source ${SRCIP}
elif [ "$1" = "stop" ]; then
	echo "Removing iptables entries"
	
	iptables -t nat -D POSTROUTING --out-interface ${DSTIFACE} -j MASQUERADE  
	iptables -D FORWARD --in-interface ${SRCIFACE} -j ACCEPT
	
	iptables -t nat -D PREROUTING -d ${SRCIP} -j DNAT --to-destination ${DSTIP}
	iptables -t nat -D POSTROUTING -s ${DSTIP} -j SNAT --to-source ${SRCIP}
else
	echo "iptables entries for NAT"
	iptables -t nat -L
fi