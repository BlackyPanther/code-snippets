/**
 * input-validation.js
 * (c) Andreas Mueller <webmaster@am-wd.de>
 */

if (typeof jQuery == 'undefined')
	throw new Error('jQuery required');

/**
 * checkField
 * 
 * Checks a field not to be empty.
 * If a pattern is given the pattern match is checked.
 * 
 * @param  object  field    The jQuery object of an input field.
 * @param  string  pattern  The regex pattern to match with.
 * 
 * @return  bool  true if the field is not empty and matches the pattern, otherwise false.
 */
function checkField(field, pattern)
{
	switch ($(field).attr('type'))
	{
		case 'button':
		case 'submit':
			return true;
		case 'checkbox':
		case 'radio':
			return $(field).is(':checked');
		default:
			if (pattern === undefined)
			{
				return $(field).val().trim() !== '';
			}
			else
			{
				var regex = new RegExp(pattern);
				return regex.test($(field).val());
			}
	}
}

/**
 * checkForm
 * 
 * Checks all given fields for an input.
 * 
 * @param  array  fields  An array of jQuery objects of the inputs to check.
 * 
 * @return  array  An array containing the errors. Empty array on successful check without error.
 * 
 * Input-Example:
 * $('input.required') => All inputs containing the CSS class 'required'.
 */
function checkForm(fields)
{
	var error = [];
	
	$.each($(fields), function(key, value)
	{
		if (!$(value).is(':visible') || $(value).text().trim() === '')
			continue;
		
		if ($(value).attr('required-id') === undefined)
		{
			//error[error.length] = $(value).text() + " | Attribute 'required-id' missing.";
			console.log($(value).text() + " | Attribute 'required-id' missing.");
			continue;
		}
		
		var ids = $(value).attr('required-id').split(',');
		var pattern = $(value).attr('regex');
		
		$.each(ids, function(k, val)
		{
			val = val.toString();
			var pat = (pattern === undefined) ? $(value).attr('regex' + k) : pattern;
			
			var checked = false;
			var ors = val.split('|');
			
			$.each(ors, function(i, v)
			{
				v = v.toString();
				if (checkField($('#' + v), pat))
					checked = true;
			});
			
			if (!checked)
			{
				error[error.length] = val;
			}
		});
	});
	
	return error;
}