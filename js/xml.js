/**
 * xml.js
 * (c) Andreas Mueller <webmaster@am-wd.de>
 */

if (typeof jQuery == 'undefined')
	throw new Error('jQuery is required');

function rec_2xml(rootName, obj)
{
	var xml = '';
	
	if (typeof obj == 'string' || typeof obj == 'number')
	{
		xml += '<' + rootNode + '>' + obj + '</' + rootNode + '>';
	}
	else if (jQuery.isArray(obj))
	{
		jQuery.each(obj, function(idx, itm)
		{
			xml += rec_2xml(rootName, itm);
		});
	}
	else if (typeof obj == 'object')
	{
		xml += '<' + rootName + '>';
		jQuery.each(obj, function(idx, itm)
		{
			xml += rec_2xml(idx, itm);
		});
		xml += '</' + rootName + '>';
	}
	
	return xml;
}

function object2xml(rootName, obj)
{
	var xml = '<?xml version="1.0" encoding="utf-8"?>';
	xml += rec_2xml(rootName, obj);
	return xml;
}



function rec_2obj(xml)
{
	if (xml.children().size() > 0)
	{
		var obj = {};
		
		xml.children().each(function()
		{
			var chObj = (jQuery(this).children().size() > 0) ? rec_2obj(jQuery(this)) : jQuery(this).text();
			if (jQuery(this).siblings().size() > 0 && jQuery(this).siblings().get(0).tagName == this.tagName)
			{
				if (obj[this.tagName] === null)
				{
					obj[this.tagName] = [];
				}
				obj[this.tagName].push(chObj);
			}
			else
			{
				obj[this.tagName] = chObj;
			}
		});
		
		return obj;
	}
	else
	{
		return xml.text();
	}
}

function xml2object(xml, rootNode)
{
	var obj = {};
	
	if (xml.find(rootNode).size() > 0)
	{
		var res = rec_2obj(xml.find(rootNode));
		obj.type = rootNode;
		obj.root = res;
	}
	else
	{
		obj.type = 'unknown';
	}
	
	return obj;
}


